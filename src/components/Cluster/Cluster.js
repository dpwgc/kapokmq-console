const nodeTitle = [{
	title: 'Name',
	dataIndex: 'Name',
	width: 300,
}, {
	title: 'Addr',
	dataIndex: 'Addr',
	width: 300,
}, {
	title: 'Port',
	dataIndex: 'Port',
}, ];

import axios from 'axios';
import VueAxios from 'vue-axios';

export default {
	data() {
		return {
			
			//安全访问密钥
			secretKey: '',
			//消息队列地址
			address: '',
			
			//集群节点列表
			nodeList: [],
			nodeTitle,
		};
	},
	mounted: function() {
		// 在页面加载时请求数据
		setTimeout(() => {
			this.secretKey = sessionStorage.getItem('secretKey');
			this.address = window.location.origin
			sessionStorage.setItem('address', this.address);
			this.GetNodes();
		}, 100);
	},
	methods: {
		//获取集群内的消息队列节点列表
		GetNodes(){
			this.$postform('/Console/GetNodes', null)
				.then(res => {
					this.nodeList = res;
				})
		},
	},
}

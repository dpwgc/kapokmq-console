import axios from 'axios';
import VueAxios from 'vue-axios';

export default {
	data() {
		return {
			//服务端是否正常运行
			isPing: false,

			//安全访问密钥
			secretKey: '',
			//消息队列地址
			address: '',

			//配置
			config: '',
		};
	},
	mounted: function() {
		// 在页面加载时请求数据
		setTimeout(() => {
			this.secretKey = sessionStorage.getItem('secretKey');
			this.address = window.location.origin
			sessionStorage.setItem('address', this.address);
			//检查连接情况
			this.ping();
			this.GetConfig();
		}, 100);
	},
	methods: {
		//将连接信息缓存在本地
		save() {
			sessionStorage.setItem('secretKey', this.secretKey);
			//检查连接情况
			this.ping();
			this.$router.push('/Console');
			this.$router.go(0);
		},
		//清除登录信息
		clean() {
			this.secretKey = "";
			this.address = "";
			sessionStorage.setItem('secretKey', "");
			this.$message.success('Cleanup succeeded');
		},
		//检查连接情况
		ping() {
			this.$postform('/Console/Ping')
				.then(res => {

					if (res.code == 0) {
						this.$message.success('Connection succeeded');
						this.isPing = true;
						this.GetConfig();
					} else {
						this.$message.error('Not online');
						this.isPing = false;
					}
				})
		},
		//获取消息队列配置
		GetConfig() {

			this.$postform('/Console/GetConfig')
				.then(res => {

					if (res.code == 0) {
						this.config = res.data;
					} else {
						this.$message.error('Error');
					}

				})
		},
	},
}

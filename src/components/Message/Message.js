const messageTitle = [{
		title: 'MessageCode',
		dataIndex: 'MessageCode',
		width: 280,
	},
	{
		title: 'Topic',
		dataIndex: 'Topic',
		width: 100,
	},
	{
		title: 'CreateTime',
		dataIndex: 'CreateTime',
		width: 160,
	},
	{
		title: 'DelayTime',
		dataIndex: 'DelayTime',
		width: 90,
	},
	{
		title: 'ConsumedTime',
		dataIndex: 'ConsumedTime',
		width: 160,
	},
	{
		title: 'Status',
		dataIndex: 'Status',
		width: 60,
	},
	{
		title: 'Operation',
		key: 'action',
		scopedSlots: {
			customRender: 'action'
		},
	},
];

import axios from 'axios';
import VueAxios from 'vue-axios';

export default {
	data() {
		return {

			//安全访问密钥
			secretKey: '',
			//消息队列地址
			address: '',

			//消息列表
			messageList: [],

			//消息表格头部
			messageTitle,

			//消息详情页
			message: '',
			visible: false,

			//搜索消息
			startTime: '',
			endTime: '',
			topic: '',
			status: 3,
			tip: 'All',

			//搜索按钮加载
			loading: false,
		};
	},
	mounted: function() {
		// 在页面加载时请求数据
		setTimeout(() => {
			this.secretKey = sessionStorage.getItem('secretKey');
			this.address = window.location.origin
			sessionStorage.setItem('address', this.address);
		}, 100);
	},
	methods: {
		onSelect({
			key
		}) {
			this.status = key;
			switch (this.status) {
				case "-1":
					this.tip = "Unconsumed";
					break;
				case "0":
					this.tip = "delay";
					break;
				case "1":
					this.tip = "Consumed";
					break;
				case "3":
					this.tip = "All";
					break;
			}
		},
		//搜索消息
		selectMessage() {

			this.loading = true;

			if (this.startTime === "") {
				this.startTime = new Date();
				return;
			}
			if (this.endTime === "") {
				this.endTime = new Date();
				return;
			}
			let data = new FormData();

			data.append("startTime", this.timeUpt(this.startTime));

			data.append("endTime", this.timeUpt(this.endTime));

			data.append("topic", this.topic);

			data.append("status", this.status);

			this.$postform('/Console/GetMessageList', data)
				.then(res => {

					if (res.code == 0) {
						this.messageList = res.data;
						for (let i in this.messageList) {
							this.messageList[i].CreateTime = this.timestampToTime(this.messageList[i].CreateTime);
							this.messageList[i].ConsumedTime = this.timestampToTime(this.messageList[i]
								.ConsumedTime);
						}
						this.loading = false;
					} else {
						this.loading = false;
						this.$message.error('Error');
					}

				})
		},

		//获取消息详情
		getMessage(e) {
			let data = new FormData();

			data.append("messageCode", e);

			this.$postform('/Console/GetMessage', data)
				.then(res => {

					if (res.code == 0) {
						this.message = res.data;
						this.message.CreateTime = this.timestampToTime(this.message.CreateTime);
						this.message.ConsumedTime = this.timestampToTime(this.message.ConsumedTime);
						this.showModal();
					} else {
						this.$message.error('Error');
					}

				})
		},

		//删除消息
		delMessage(e) {
			let data = new FormData();

			data.append("messageCode", e);

			this.$postform('/Console/DelMessage', data)
				.then(res => {

					if (res.code == 0) {
						for (let i in this.messageList) {
							if (this.messageList[i].MessageCode == this.message.MessageCode) {
								this.messageList[i].MessageCode = "已删除";
								break;
							}
						}
						this.$message.success('Deleted successfully');
					} else {
						this.$message.error('Error');
					}

				})
		},

		//消息详情弹框
		showModal() {
			this.visible = true;
		},
		hideModal() {
			this.visible = false;
		},

		//时间格式转换
		timeUpt(time) {
			let yy = new Date(time).getFullYear();
			let mm = new Date(time).getMonth() + 1;
			if (mm < 10) {
				mm = "0" + mm
			}
			let dd = new Date(time).getDate();
			if (dd < 10) {
				dd = "0" + dd
			}
			let hh = new Date(time).getHours();
			if (hh < 10) {
				hh = "0" + hh
			}
			let mf = new Date(time).getMinutes();
			if (mf < 10) {
				mf = "0" + mf
			}
			let ss = new Date(time).getSeconds();
			if (ss < 10) {
				ss = "0" + ss
			}

			return yy + "-" + mm + "-" + dd + " " + hh + ":" + mf + ":" + ss;
		},
		//获取前aa天的日期
		latelyDateTime(aa) {
			var date1 = new Date(),
				time1 = date1.getFullYear() + "-" + (date1.getMonth() + 1) + "-" + date1.getDate(); //time1表示当前时间  
			var date2 = new Date(date1);
			date2.setDate(date1.getDate() + aa);
			var time2 = date2.getFullYear() + "-" + (date2.getMonth() + 1) + "-" + date2.getDate();
			return time2;
		},
		//时间戳转日期字符串
		timestampToTime(timestamp) {
			var date = new Date(timestamp * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
			var Y = date.getFullYear() + '-';
			var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
			var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
			var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
			var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
			var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
			return Y + M + D + h + m + s;
		},
	},
}

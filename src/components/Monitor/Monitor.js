const consumerTitle = [{
	title: 'ConsumerId',
	dataIndex: 'ConsumerId',
	width: 200,
}, {
	title: 'Topic',
	dataIndex: 'Topic',
	width: 200,
}, {
	title: 'JoinTime',
	dataIndex: 'JoinTime',
	width: 200,
}, {
	title: 'ConsumerIp',
	dataIndex: 'ConsumerIp',
}, ];
const producerTitle = [{
	title: 'ProducerId',
	dataIndex: 'ProducerId',
	width: 200,
}, {
	title: 'Topic',
	dataIndex: 'Topic',
	width: 200,
}, {
	title: 'JoinTime',
	dataIndex: 'JoinTime',
	width: 200,
}, {
	title: 'ProducerIp',
	dataIndex: 'ProducerIp',
}, ];

import axios from 'axios';
import VueAxios from 'vue-axios';
import F2 from "@antv/f2"; //引入插件

export default {
	data() {
		return {

			//安全访问密钥
			secretKey: '',
			//消息队列地址
			address: '',

			//消息列表
			messageList: [],
			//最近七天的消息列表
			latelyMessageList: [],

			//消费者客户端列表
			consumers: [],
			//生产者客户端列表
			producers: [],
			//生产者客户端数量
			producerNum: 0,
			//消费者客户端数量
			consumerNum: 0,
			
			//消息数量统计
			count: {},
			all: 0,
			consumed: 0,
			unconsumed: 0,
			delay: 0,

			//消费者表格头部
			consumerTitle,
			//生产者表格头部
			producerTitle,

			visible: false,
		};
	},
	mounted: function() {
		// 在页面加载时请求数据
		setTimeout(() => {
			this.secretKey = sessionStorage.getItem('secretKey');
			this.address = window.location.origin
			sessionStorage.setItem('address', this.address);
			this.GetClients();
		}, 100);
	},
	methods: {
		//获取客户端列表
		GetClients() {

			this.$postform('/Console/GetClients')
				.then(res => {

					if (res.code == 0) {

						//消费者列表
						this.consumers = res.consumers;
						for (let i in this.consumers) {
							this.consumers[i].JoinTime = this.timestampToTime(this.consumers[i].JoinTime);
						}
						this.consumerNum = res.consumers.length;

						//生产者列表
						this.producers = res.producers;
						for (let i in this.producers) {
							this.producers[i].JoinTime = this.timestampToTime(this.producers[i].JoinTime);
						}
						this.producerNum = res.producers.length;

					} else {
						this.$message.error('Error');
					}

				})
		},
		//搜索最近一周的消息
		selectLatelyMessage() {

			let data = new FormData();

			data.append("startTime", this.latelyDateTime(-7) + " 00:00:00");
			data.append("endTime", this.timeUpt(new Date()));
			data.append("status", 3);

			this.$postform('/Console/GetMessageList', data)
				.then(res => {

					if (res.code == 0) {
						this.latelyMessageList = res.data;
						this.showChart();
					} else {
						this.$message.error('Error');
					}

				})
		},
		//获取各状态消息数量
		CountMessage() {

			this.count = {}

			this.$postform('/Console/CountMessage', null)
				.then(res => {

					if (res.code == 0) {
						this.count = res.data;
						this.all = this.count.all;
						this.consumed = this.count.consumed;
						this.unconsumed = this.count.unconsumed;
						this.delay = this.count.delay;
					} else {
						this.$message.error('Error');
					}

				})
		},
		//@antv/f2线性图表数据初始化
		showChart() {

			var data = [];
			
			var latelyMessageList = this.latelyMessageList;

			for (let i in latelyMessageList) {

				var time = this.timestampToTime(latelyMessageList[i].CreateTime).split(" ")[0];

				var flag = false;

				//日期去重，统计每天的提交数量
				for (let j in data) {
					//如果找到重复的日期，数量增加
					if (time === data[j].date) {
						data[j].value = data[j].value + 1;
						flag = true;
						break;
					}
				}
				//不重复的日期，加入data数组
				if (flag == false) {
					var d = {
						date: time,
						value: 1
					}
					data.push(d);
				} else {
					flag = false;
				}
			}
			var chart = new F2.Chart({
				id: "mountNode",
				pixelRatio: window.devicePixelRatio
			});

			chart.source(data, {
				value: {
					tickCount: 5,
					min: 0
				},
				date: {
					type: "timeCat",
					range: [0, 1, 2, 3, 4, 5, 6],
					tickCount: 1
				}
			});
			chart.tooltip({
				custom: true,
				showXTip: true,
				showYTip: true,
				snap: true,
				crosshairsType: "xy",
				crosshairsStyle: {
					lineDash: [2]
				}
			});
			chart.axis("date", {
				label: function label(text, index, total) {
					var textCfg = {};
					if (index === 0) {
						textCfg.textAlign = "left";
					} else if (index === total - 1) {
						textCfg.textAlign = "right";
					}
					return textCfg;
				}
			});
			chart
				.line()
				.position("date*value")
				.style({
					stroke: "#f5222d",
					lineWidth: 3
				});
			chart.render();
		},

		//数据分析抽屉
		afterVisibleChange(val) {
			console.log('visible', val);
		},
		showDrawer() {
			this.CountMessage();
			this.selectLatelyMessage();
			this.visible = true;
		},
		onClose() {
			this.visible = false;
		},

		//时间格式转换
		timeUpt(time) {
			let yy = new Date(time).getFullYear();
			let mm = new Date(time).getMonth() + 1;
			if (mm < 10) {
				mm = "0" + mm
			}
			let dd = new Date(time).getDate();
			if (dd < 10) {
				dd = "0" + dd
			}
			let hh = new Date(time).getHours();
			if (hh < 10) {
				hh = "0" + hh
			}
			let mf = new Date(time).getMinutes();
			if (mf < 10) {
				mf = "0" + mf
			}
			let ss = new Date(time).getSeconds();
			if (ss < 10) {
				ss = "0" + ss
			}

			return yy + "-" + mm + "-" + dd + " " + hh + ":" + mf + ":" + ss;
		},
		//获取前aa天的日期
		latelyDateTime(aa) {
			var date1 = new Date(),
				time1 = date1.getFullYear() + "-" + (date1.getMonth() + 1) + "-" + date1.getDate(); //time1表示当前时间  
			var date2 = new Date(date1);
			date2.setDate(date1.getDate() + aa);
			var time2 = date2.getFullYear() + "-" + (date2.getMonth() + 1) + "-" + date2.getDate();
			return time2;
		},
		//时间戳转日期字符串
		timestampToTime(timestamp) {
			var date = new Date(timestamp * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
			var Y = date.getFullYear() + '-';
			var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
			var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
			var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
			var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
			var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
			return Y + M + D + h + m + s;
		},
	},
}

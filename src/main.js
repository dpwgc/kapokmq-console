import Vue from 'vue'
import App from './App.vue'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';

import axios from 'axios';
import VueAxios from 'vue-axios';

import {
	postform
} from './request.js';

import router from './router';
import Router from 'vue-router';

Vue.use(Antd);
Vue.use(VueAxios, axios)
Vue.use(Router)

Vue.config.productionTip = false

Vue.prototype.$postform = postform;

new Vue({
	router,
	render: h => h(App),
}).$mount('#app')

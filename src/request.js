import axios from 'axios';
import {
	baseUrl
} from './main.js';

axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

var instance = axios.create({
  headers: {'content-type':'multipart/form-data'}
});

/**
 * 封装post请求 FormData方式
 * @param url
 * @param data
 * @returns {Promise}
 */

export function postform(uri, data = {}) {

	return new Promise((resolve, reject) => {
		
		var secretKey = sessionStorage.getItem('secretKey');
		var address = sessionStorage.getItem('address');
		//var address = "http://127.0.0.1:8011"
		
		var instance = axios.create({
			headers: {
				'secretKey': secretKey,
				'content-type': 'multipart/form-data'
			}
		});
		
		instance.post(address + uri, data)
			.then(response => {

				if (response.data.code == -1) {
					this.$message.error(response.data.msg);
					return;
				}
				resolve(response.data);
			}, err => {
				reject(err)
			})
	})
}

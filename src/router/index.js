import Vue from 'vue'
import Router from 'vue-router'
//组件
import Configure from "../components/Configure/Configure.vue";
import Monitor from "../components/Monitor/Monitor.vue";
import Message from "../components/Message/Message.vue";
import Cluster from "../components/Cluster/Cluster.vue";

Vue.use(Router)

export default new Router({
	mode: "hash",
	routes: [
		{
			path: '/Console',
			component: Configure,
		},
		{
			path: '/Console/Monitor',
			component: Monitor,
		},
		{
			path: '/Console/Message',
			component: Message,
		},
		{
			path: '/Console/Cluster',
			component: Cluster,
		},
	]
})
